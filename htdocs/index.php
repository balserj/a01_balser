<!DOCTYPE html>
<html>
<head>
    <title>CS3140 project A01 for Justin Balser</title>
    <h1 style="text-align:center">Assignment 01 -- Balser, Justin</h1>
    <meta charset = "UTF-8">

</head>

<body>

<!--Summary: This section should include a basic biography and any interesting facts about yourself that you’d like to include. This should be in paragraph format.-->

    div>
        <p>
        My name is Justin Balser and I'm currently a Junior at BGSU, majoring in Computer Science and love to play Sports video games and Pokemon Go.
        </p>
    </div>

<!-- Personal Information: This should include name, address, phone number, email address.-->

    <div>
        <p>
        Name: Justin Balser
        Address: 1097 Cinnamon Lane, Lewis Center, OH, 43035
        Phone Number: 274-867-5309
        Email Address: balserj@bgsu.edu
        </p>
    </div>

<!-- Academic Information: List all of your high school and college information including any particular notes about classes that you have taken. You may include future education that
  you hope to have as well.-->

    <div>
        <p>
	High School:
	4 years of Social Studies, Math, English/Language Arts, and Science at Bishop Watterson High School.
	2 years of Spanish and Computer related courses
	College:
	Hoping to obtain a Bachelors of Science in Computer Science as the highest level of education I want to obtain unless a better opportunity presents itself.
        </p>
    </div>

<!-- Employment Information: List any employment past, present, or future. At least three.-->

    <div>
        <p>Employment Information:</p>
        <ul>
        <li>Past Places of Employment: McDonald's</li>
        <li>Present Places of Employment: Meijer</li>
        <li>Future Places of Employment: JPMorgan Chase</li>
        </ul>
    </div>

</body>
</html>	